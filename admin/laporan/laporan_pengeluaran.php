<?php

include 'config.php';
require('../laporan/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);

$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'Inventory Rumah Sakit',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 082165461929',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL.Porsea',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.InventoriRS.com email : ridhopangaribuan04@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(0,0.7,'Laporan Data Pengeluaran Barang',0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->Cell(6,0.7,"Laporan Pengeluaran pada : ".@$_GET['tanggal'],0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');

$pdf->Cell(6, 0.8, 'Kode Barang', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Jumlah Barang', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tujuan', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal', 1, 1, 'C');	
$pdf->SetFont('Arial','',10);

$no = 1;

$tanggal=@$_GET['tanggal'];
$link=mysqli_connect("localhost","root","","inventori");
$result=mysqli_query($link, "select * from barang_keluar");

while($lihat=mysqli_fetch_array($result)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	
	$pdf->Cell(6, 0.8, $lihat['kode_barang'],1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['nama_barang'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['jumlah'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['tujuan'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal'], 1, 1,'C');
	
	$no++;
}
$pdf->Output("laporan_pengeluaran.pdf","I");

?>

