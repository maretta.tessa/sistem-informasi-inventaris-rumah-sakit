<?php
session_start();

# check apakah ada akse post dari halaman login?, jika tidak kembali kehalaman depan
if( !isset($_POST['username']) ) { header('location:index.php'); exit(); }

# set nilai default dari error,
$error = '';

require ( 'config.php' );

$username = trim( $_POST['username'] );
$password = trim( $_POST['password'] );
function encrypt_decrypt($action, $string) {
				$output = false;
				$encrypt_method = "AES-256-CBC";
				$secret_key = 'This is my secret key';
				$secret_iv = 'This is my secret iv';
				// hash
				$key = hash('sha256', $secret_key);
    
				// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
				$iv = substr(hash('sha256', $secret_iv), 0, 16);
				if ( $action == 'encrypt' ) {
					$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
					$output = base64_encode($output);
				} else if( $action == 'decrypt' ) {
					$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
				}
				return $output;
}
//qqq
$encrypted_txt = encrypt_decrypt('encrypt', $password);
$password= $encrypted_txt;
$sql = mysqli_query("Insert Into login(username,password) VALUES ('$username','$password')");

if( strlen($username) < 2 )
{
	# jika ada error dari kolom username yang kosong
	$error = "<script language='javascript'> alert('Username tidak boleh kosong'); </script>";
}else{

	# Escape String, ubah semua karakter ke bentuk string
	$username = $koneksi->escape_string($username);
	$password = $koneksi->escape_string($password);


	# SQL command untuk memilih data berdasarkan parameter $username dan $password yang 
	# di inputkan
	$sql = "SELECT nama, level FROM users 
			WHERE username='$username' 
			AND password='$password' LIMIT 1";

	# melakukan perintah
	$query = $koneksi->query($sql);

	# check query
	if( !$query )
	{
		die( 'Oops!! Database gagal '. $koneksi->error );
	}

	# check hasil perintah
	if( $query->num_rows == 1 )
	{	
		# jika data yang dimaksud ada
		# maka ditampilkan
		$row =$query->fetch_assoc();
		
		# data nama disimpan di session browser
		$_SESSION['user'] = $row['nama']; 
		$_SESSION['akses']	   = $row['level'];

		if( $row['level'] == 'admin')
		{
			# data hak Admin di set
			$_SESSION['admin']= 'TRUE';
		}

		# menuju halaman sesuai hak akses
		header('location:'.$url.''.$_SESSION['akses'].'');
		exit();

	}else{
		
		# jika data yang dimaksud tidak ada
		$error = "<script language='javascript'> alert('Username atau password salah !'); </script>";
	}

}

if( !empty($error) )
{
	# simpan error pada session
	$_SESSION['error'] = $error;
	header('location:'.$url.'index.php');
	exit();
}
?>